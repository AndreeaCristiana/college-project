function deschidereNavigatie() {
  document.getElementById("deschidere-buton").style.display = "none";
  document.getElementById("continut-pagini-transparenta").style.visibility = "visible";
  document.getElementById("nav-pagini").style.width = "250px";
  document.getElementById("continut").style.marginLeft = "250px";
}

function inchidereNavigatie() {
  document.getElementById("deschidere-buton").style.display = "inline-flex";
  document.getElementById("continut-pagini-transparenta").style.visibility = "hidden";
  document.getElementById("nav-pagini").style.width = "0";
  document.getElementById("continut").style.marginLeft = "0";
}